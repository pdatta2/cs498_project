import rsa
import hashlib
import json
from time import time
from urllib.parse import urlparse
from uuid import uuid4
from base64 import b64encode, b64decode

def gen_keys():
    keysize = 1024
    (public, private) = rsa.newkeys(keysize)
    return (public,private)

(pub_key, secret) = gen_keys()
pub_key_str = b64encode(pub_key.exportKey())

class Blockchain:
    def __init__(self, node_ID):
        #self.current_transactions = []
        self.chain = []
        self.nodes = {}
        self.blocked_nodes = []
        self.nodeID = node_ID
        # Create the genesis block
        self.new_block(previous_hash='1', logdata = "" )
        
    def new_block(self, previous_hash, logdata):
        """
        Create a new Block in the Blockchain
        :param previous_hash: Hash of previous Block
        :logdata: list of events (e1, seq_no)
        :return: New Block
        """

        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'data': logdata,
            'from': self.nodeID,
            'signature': signLog(logdata),
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
        }

        self.chain.append(block)
        return block
    
    def add_received_block(block):
        
        sender = block['from']
        timestamp = block['timestamp']
        data = block['data']
        signature = block['signature']
        previous_hash = block['previous_hash']
        
        # it can be assumed that the sgx assigns sequence numbers to the set of log events
        # and signs it, so ne need to check if all sequence numbers are present
        
        # verify if in blocked_nodes then don't add
        if(sender in self.blocked_nodes):
            return False
        
        # verify signature - not matched return false
        sender_key = self.nodes['sender'][1]
        if(not verifyLogsign(data, signature, sender_key)):
            return False
        
        # verify previous hash - if matched add, else resolve conflict by other chains
        # Then check again if the hash matches
        prev_hash = self.hash(self.chain[-1])
        if(previous_hash != prev_hash):
            changed = self.resolve_conflicts(sender)
            if(not(changed and self.hash(self.chain[-1])==prev_hash)): # hash is not matching
                return False
               
        # verify timestamp (time duration) for log block from the same sender and
        # check sequence numbers by events in logdata and check if it matches sequence from last 
        # log from the same node
        current_index = len(self.chain)-1
        
        while(current_index>0):
            current_block = self.chain[current_index]
            if(current_block['from']==sender):
                break;
        
        last_timestamp = current_block['timestamp']
        
        if(timestamp - last_timestamp > threshold):
            return False
        
        last_logdata = current_block['data']
        logtuple = last_logdata[-1]
        seq = logtuple[1]
        
        logtuple_toadd = data[0]
        seq_toadd = logtuple_toadd[1]
        
        if(seq+1 != seq_toadd):
            return False
        
        # if all checks succees add the block
        self.chain.append(block)
        print("Block from " + sender +" added.")
        return True
        
    
    
    def commitBlock(self, logData):
        """
        Adds a new block to its own chain and 
        broadcast the block to peers
        """
        previous_hash = self.hash(self.chain[-1])
        block = self.new_block(previous_hash,logData)
        block_json = json.dumps(block)
        
        neighbours = self.nodes
        
        for node in neighbours:
            response = requests.post(f'http://{node}/add_block',data = block_json)
            
    
    def register_node(self, node):
        """
        Add a new node to the list of nodes
        :param address: Address of node. Eg. 'http://192.168.0.5:5000'
        """
        node_id = node['id']
        address = node['url']
        pub_key_encoded = b64decode(node['pub_key'])
        pub_key = rsa.read_key(pub_key_encoded.encode('utf-8'))
        parsed_url = urlparse(address)
        self.nodes[node_id] = [parsed_url.netloc, pub_key]
        
    def add_to_blocked_list(self, node):
        # deletes suspected node from peer list
        self.blocked_nodes.append(node)
        del self.nodes[node]
        
    def valid_chain(self, chain):
        """
        Determine if a given blockchain is valid
        :param chain: A blockchain
        :return: True if valid, False if not
        """

        last_block = chain[0]
        current_index = 1

        while current_index < len(chain):
            block = chain[current_index]
            print(f'{last_block}')
            print(f'{block}')
            print("\n-----------\n")
            # Check that the hash of the block is correct
            if block['previous_hash'] != self.hash(last_block):
                return False

            # Check that the Proof of Work is correct

            last_block = block
            current_index += 1

        return True

    def resolve_conflicts(self, sender):
        """
        This is our consensus algorithm, it resolves conflicts
        by replacing our chain with the longest one in the network.
        :return: True if our chain was replaced, False if not
        """

        neighbours = []
        new_chain = None
        
        for key, value in self.nodes.items():
            if(key!=sender):
                neighbours.append(value[0]) #first item in value-list is url

        # We're only looking for chains longer than ours
        max_length = len(self.chain)

        # Grab and verify the chains from all the nodes in our network
        for node in neighbours:
            response = requests.get(f'http://{node}/chain')

            if response.status_code == 200:
                length = response.json()['length']
                chain = response.json()['chain']

                # Check if the length is longer and the chain is valid
                if length > max_length and self.valid_chain(chain):
                    max_length = length
                    new_chain = chain

        # Replace our chain if we discovered a new, valid chain longer than ours
        if new_chain:
            self.chain = new_chain
            return True

        return False

    
    
    @property
    def last_block(self):
        return self.chain[-1]
        
    @staticmethod
    def hash(block):
        """
        Creates a SHA-256 hash of a Block
        :param block: Block
        """
        # We must make sure that the Dictionary is Ordered, or we'll have inconsistent hashes
        block_string = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(block_string).hexdigest()

def signLog(logData):
    msg = "".join(s for log in logData for s in log)
    msg = msg.encode('utf-8')
    signature = b64encode(rsa.sign(msg, secret, "SHA-512"))
    return signature.decode('utf-8')

def verifyLogsign(logData, signature, pub_key):
    msg = "".join(s for log in logData for s in log)
    return verify(msg, b64decode(signature.encode('utf-8')), pub_key)

