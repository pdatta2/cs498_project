from flask import Flask
from flask import request
import json
import requests
import hashlib as hasher
import datetime as date
from LogStore import commit_current_log

node = Flask(__name__)


# Store the transactions that this node has in a list
this_nodes_transactions = []

peer_nodes = []
blockchain = []
blockchain.append(create_genesis_block())

# A variable to decide if this node adds log events
mining = True

class Block:
	def __init__(self, index, timestamp, data, previous_hash):
		self.index = index
		self.timestamp = timestamp
		self.data = data
		self.previous_hash = previous_hash
		self.hash = self.hash_block()
  
  	def hash_block(self):
		sha = hasher.sha256()
		sha.update(str(self.index) + 
			str(self.timestamp) + 
			str(self.data) + 
			str(self.previous_hash))
			return sha.hexdigest()

def create_genesis_block():
	# Manually construct a block with
	# index zero and zero previous hash
	return Block(0, date.datetime.now(), "Genesis Block", "0")

	
def next_block(last_block):
	this_index = last_block.index + 1
	this_timestamp = date.datetime.now()
	this_data = commit_current_log()
	this_hash = last_block.hash
	return Block(this_index, this_timestamp, this_data, this_hash)


@node.route('/newlog', methods=['POST'])
def add_new_logs():
	new_log_block = request.get_json()
	this_nodes_transactions.append(new_log_block)
	print "New transaction"
	print "FROM: {}".format(new_log_block['from'])
	return "Committing logs to chain successful\n"

@node.route('/blocks', methods=['GET'])
def get_blocks():
	chain_to_send = blockchain

	for i in range(len(chain_to_send)):
		block = chain_to_send[i]
		block_index = str(block.index)
		block_timestamp = str(block.timestamp)
		block_data = str(block.data)
		block_hash = block.hash
		chain_to_send[i] = {
			"index": block_index,
			"timestamp": block_timestamp,
			"data": block_data,
			"hash": block_hash
			}
	chain_to_send = json.dumps(chain_to_send)
	return chain_to_send

def find_new_chains():
	other_chains = []
	for nore_url in peer_nodes:
		block = requests.get(node_url + "/blocks").content
		block = json.loads(block)
		other_chains.append(block)
	return other_chains


def consensus():
	
	other_chains = find_new_chains()
	longest_chain = blockchain
	for chain in other_chains:
		if len(longest_chain) < len(chain):
			longest_chain = chain
	blockchain = longest_chain





if __name__ == '__main__':
    node.run(debug=True)
