import threading
import time
import os
import queue
import requests
import LogChain
from uuid import uuid4
from flask import Flask, jsonify, request
import json

#define the constants
block_size = 8
timer_duration = 15
log_read_interval = 3

#instantiate the server and blockchain
app = Flask(__name__)
nodeID = str(uuid4()).replace('-', '')
blockchain = LogChain.Blockchain(nodeID)
node_list = ['localhost:5000','localhost:5001']       


class LogMiner(object):
    """ Threading example class
    The run() method will be started and it will run in the background
    until the application exits.
    """
    def __init__(self, filename):
        """ 
        Constructor
        filename is a string parameter defining the logfile for this node
        
        """
        self.logfile = filename
        self.logdata = []
        self.msgQ = queue.Queue()
        self.timeout = threading.Event()
        
        loggerThread = threading.Thread(target=self.readlog, args=())
        loggerThread.start()                                  # Start the execution
        
        timerThread = threading.Thread(target = self.timer, args=())
        timerThread.start()
        
    def timer(self):
        while True:
            isIntr = self.timeout.wait(timer_duration)
            
            if(isIntr):                   # interrupted by other thread
                self.timeout.clear()      # Internal flag is cleared to be interrupted again
                continue
            else:                         # timer has gone off            
                                          # signal the other thread about it by
                self.msgQ.put('timeup')   # sending msg in msgQ 
                self.msgQ.join()          # and waiting for logger to get the msg
    
    def readlog(self,interval = log_read_interval):
        """ 
        Method that reads logs and mine blocks forever 
        interval defines the duration for which thread waits
        for the next log event to get generated
        
        """
        with open(self.logfile, 'r', os.O_NONBLOCK) as f:
            counter = 0
            while True:
                event = f.readline().strip()
                event = event.split(",")
                if(event is ""):
                    time.sleep(interval)
                    continue
                else:
                    counter = counter + 1
                    self.logdata.append(event)
                    
                    #check whether timer is up
                    if(self.msgQ.empty()):  #timer is not up
                        # check if the block is full
                        if(counter == block_size):
                            #gettime()
                            #print(" block full: "+self.logdata)
                            blockchain.commitBlock(self.logdata)
                            self.logdata.clear()
                            counter = 0
                            self.timeout.set() # signal the timer to start again
                    else:
                        #time is up
                        msg = self.msgQ.get()
                        #gettime()
                        #print(" timeup: "+self.logdata)
                        blockchain.commitBlock(self.logdata)
                        self.logdata.clear()
                        counter = 0
                        self.msgQ.task_done()
    

def gettime():
    from datetime import datetime
    t = datetime.now().strftime("%H:%M:%S")
    print("Time :"+t)

@app.route('/add_block', methods=['POST'])
def add_advertised_block():
    block_json = request.get_json()
    block = json.loads(block_json)
    status = blockchain.add_received_block(block)
    sender = block['from']
    
    if(not status):
         #status false means block verification did not succeed
        blockchain.alert_peers(sender)
        response = {
            'message':  'Sender might be misbehaving!',
            'sender' : f'{sender}'
        }
        return jsonify(response), 401
    else:
        response = {
            'message':  'Block succesfully added',
            'node' : f'{sender}'
        }
        return jsonify(response), 201

    

@app.route('/alert', methods=['POST'])
def alert_msg():
    values = request.get_json()
    node = values.get('node')  # this node is node_id
    blockchain.add_to_blocked_list(node) # remove blocked nodes from peer list
    response = {
        'message': 'Rogue node blacklisted',
    }
    return jsonify(response), 201


@app.route('/chain', methods=['GET'])
def full_chain():
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain),
    }
    return jsonify(response), 200

@app.route('/nodes/register', methods=['POST'])
def register_nodes():
    node = request.get_json()
    print("Register API data: "+node)
    blockchain.register_node(node)

    response = {
        'message': 'New node have been added',
        'total_nodes': list(blockchain.nodes),
    }
    return jsonify(response), 201

@app.route('/nodes/resolve', methods=['GET'])
def consensus():
    replaced = blockchain.resolve_conflicts()

    if replaced:
        response = {
            'message': 'Our chain was replaced',
            'new_chain': blockchain.chain
        }
    else:
        response = {
            'message': 'Our chain is authoritative',
            'chain': blockchain.chain
        }

    return jsonify(response), 200

# define API for log reconstruction    

if __name__ == '__main__':
    # Instantiate the Node
    
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    parser.add_argument('-f', '--file', default="logfile", type=str, help='logfile to process')
    args = parser.parse_args()
    port = args.port
    file = args.file

    #run the server
    
    from multiprocessing import Process

    print("Starting server at port "+ str(port))
    server = Process(target=app.run,args=('localhost',port,))
    server.start()
    
    
    #app.run(host='localhost', port=port)
    print("It is time to sleep")
    time.sleep(10)
    
    #register itself with other nodes

    #print(LogChain.pub_key_str..decode('utf-8'))
    node_data = {
        'id':nodeID,
        'url':f'localhost:{port}',
        'pub_key':LogChain.pub_key_str.decode('utf-8'),
    }
    
    node_data_json = json.dumps(node_data)

    for node in node_list:
        port_num = node.split(":")[1]
        if(port_num!=port):
            node_url = f'http://{node}/nodes/register'
            print(node_url+":"+node_data_json)
            requests.post(node_url,data=node_data_json)

    
    # start the logMiner
    logReader = LogMiner(file)
   

