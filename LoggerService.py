import concurrent.futures
import time
import random
import os

class Logger(object):
    """ Logger class mimics Audit logging for multiple nodes.
    Spawns a thread for each node and keeps generating events
    in separate files for each node. 
    """

    def __init__(self, max_nodes = 2):
        """ Constructor
            param max_nodes: number of nodes 
        """
        self.num_nodes = max_nodes
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers = max_nodes)

    def generate_log_events(self, filename, counter = 1):
        """ Method that generates log events with sequence number
            and writes them to file forever """
        
        with open(filename, 'a', os.O_NONBLOCK) as f:
            while True:
                f.write("Log Event,"+ str(counter)+"\n")
                f.flush()
                counter = counter + 1
                #waits for next log event to occur randomly
                interval = random.randint(1,3) 
                time.sleep(interval)
                
    def start_threads(self):
        for i in range(self.num_nodes):
            filename = "logfile"+ str(i)
            self.executor.submit(self.generate_log_events,filename)


logger = Logger(2)
logger.start_threads()
