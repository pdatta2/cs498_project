import MinerNode
import subprocess
import time

if __name__=='__main__':
    node_list = ['localhost:5000','localhost:5001']
    
    logger = 'python LoggerService.py'
    
    proc1 ='python MinerNode.py -p ' + str(5000) + ' -f ' + "logfile0"
    proc2 ='python MinerNode.py -p ' + str(5001) + ' -f ' + "logfile1"

    l = subprocess.Popen(logger, shell=True)
    p1 = subprocess.Popen(proc1, shell=True)
    p2 = subprocess.Popen(proc2, shell=True)

